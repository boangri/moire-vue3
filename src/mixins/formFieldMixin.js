import BaseFormField from '@/components/BaseFormField.vue';

export default {
  name: 'FormFieldMixin',
  components: { BaseFormField },
  props: ['title', 'error', 'placeholder', 'value', 'text'],
  computed: {
    dataValue: {
      get() {
        return this.value;
      },
      set(value) {
        this.$emit('input', value);
      },
    },
  },
};
